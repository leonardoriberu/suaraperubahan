package techno.suaraperubahan.Model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class APIVotes extends APIBaseResponse implements Serializable {
    int totalVotes;

    public int getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }
}
