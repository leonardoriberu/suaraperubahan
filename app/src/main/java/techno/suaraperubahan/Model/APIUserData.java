package techno.suaraperubahan.Model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class APIUserData extends APIBaseResponse implements Serializable {
    List<UserData> userData;

    public List<UserData> getUserData() {
        return userData;
    }

    public void setUserData(List<UserData> userData) {
        this.userData = userData;
    }
}
