package techno.suaraperubahan.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by MR-ROBOT on 11/17/16.
 */

public class ApiComment extends APIBaseResponse implements Serializable {
    public List<UserComment> kontenData;

    public List<UserComment> getKontenData() {
        return kontenData;
    }

    public void setKontenData(List<UserComment> kontenData) {
        this.kontenData = kontenData;
    }
}
