package techno.suaraperubahan.Model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Flags implements Serializable {
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    String flag;
}
