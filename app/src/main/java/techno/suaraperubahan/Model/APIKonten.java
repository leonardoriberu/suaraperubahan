package techno.suaraperubahan.Model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class APIKonten extends APIBaseResponse implements Serializable {
    List<KontenData> kontenData;

    public List<KontenData> getKontenData() {
        return kontenData;
    }

    public void setKontenData(List<KontenData> kontenData) {
        this.kontenData = kontenData;
    }
}
