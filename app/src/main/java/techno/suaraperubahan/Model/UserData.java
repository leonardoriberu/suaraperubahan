package techno.suaraperubahan.Model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UserData implements Serializable {
    public void setID_User(String ID_User) {
        this.ID_User = ID_User;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public void setFull_Name(String full_Name) {
        Full_Name = full_Name;
    }

    private String ID_User;
    private String Email;
    private String Password;
    private String Full_Name;
    private String Is_Received;
    private String Date_Creation;

    public String getID_User() {
        return ID_User;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }

    public String getFull_Name() {
        return Full_Name;
    }

    public String getIs_Received() {
        return Is_Received;
    }

    public String getDate_Creation() {
        return Date_Creation;
    }
}
