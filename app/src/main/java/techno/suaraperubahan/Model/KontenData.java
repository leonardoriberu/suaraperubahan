package techno.suaraperubahan.Model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class KontenData implements Serializable {
    String ID_Petisi;
    String Email;
    String Judul_Petisi;
    String Isi_Petisi;
    String Gambar_Petisi;
    String Tags;
    String Created_at;
    int Jumlah_Dukungan;
    List<Flags> Flag;
    int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<Flags> getFlag() {
        return Flag;
    }

    public String getID_Petisi() {
        return ID_Petisi;
    }

    public void setID_Petisi(String idpetisi) {
        this.ID_Petisi = idpetisi;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public void setFlag(List<Flags> flag) {
        Flag = flag;
    }


    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getJudul_Petisi() {
        return Judul_Petisi;
    }

    public void setJudul_Petisi(String judulpetisi) {
        Judul_Petisi = judulpetisi;
    }


    public String getIsi_Petisi() {
        return Isi_Petisi;
    }

    public void setIsi_Petisi(String isipetisi) {
        Isi_Petisi = isipetisi;
    }

    public String getGambar_Petisi() {
        return Gambar_Petisi;
    }

    public void setGambar_Petisi(String gambarpetisi) {
        Gambar_Petisi = gambarpetisi;
    }

    public int getJumlah_Dukungan() {
        return Jumlah_Dukungan;
    }

    public void setJumlah_Dukungan(int jumlah_Dukungan) {
        Jumlah_Dukungan = jumlah_Dukungan;
    }

    public String getTags() {
        return Tags;
    }

    public void setTags(String tags) {
        Tags = tags;
    }

}
