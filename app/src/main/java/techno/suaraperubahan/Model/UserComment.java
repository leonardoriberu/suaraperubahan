package techno.suaraperubahan.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by MR-ROBOT on 11/17/16.
 */

public class UserComment implements Serializable {

        private String ID_Komentar;
        private String Email;
        private String ID_Petisi;

    public void setID_Komentar(String ID_Komentar) {
        this.ID_Komentar = ID_Komentar;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setID_Petisi(String idpetisi) {
        this.ID_Petisi = idpetisi;
    }

    public void setIsi_Komentar(String isi_Komentar) {
        Isi_Komentar = isi_Komentar;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    private String Isi_Komentar;
        private String Created_at;

        public String getID_Komentar() {
            return ID_Komentar;
        }

        public String getEmail() {
            return Email;
        }

        public String getID_Petisi() {
            return ID_Petisi;
        }

        public String getIsi_Komentar() {
            return Isi_Komentar;
        }

        public String getCreated_At() {
            return Created_at;
        }

}
