package techno.suaraperubahan.Model;

import java.util.List;

public class APIHistory extends APIBaseResponse {
    List<History> historyData;

    public List<History> getHistoryData() {
        return historyData;
    }

    public void setHistoryData(List<History> historyData) {
        this.historyData = historyData;
    }
}
