package techno.suaraperubahan.Model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class APIBaseResponse implements Serializable {
    int status;
    String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
