package techno.suaraperubahan.Model;

public class History {
    String ID_History, Email, Hadiah, Redeemed_at;

    public String getID_History() {
        return ID_History;
    }

    public void setID_History(String ID_History) {
        this.ID_History = ID_History;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getHadiah() {
        return Hadiah;
    }

    public void setHadiah(String hadiah) {
        Hadiah = hadiah;
    }

    public String getRedeemed_at() {
        return Redeemed_at;
    }

    public void setRedeemed_at(String redeemed_at) {
        Redeemed_at = redeemed_at;
    }
}
