package techno.suaraperubahan.Rest;

import android.text.Editable;

import techno.suaraperubahan.Helper.ToStringConverter;
import techno.suaraperubahan.Model.APIBaseResponse;
import techno.suaraperubahan.Model.APIHistory;
import techno.suaraperubahan.Model.APIKonten;
import techno.suaraperubahan.Model.APIUserData;
import techno.suaraperubahan.Model.APIVotes;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;
import techno.suaraperubahan.Model.ApiComment;

public class RestClient {

    private static GitApiInterface gitApiInterface ;
    private static String baseUrl = "http://hendririberu.com" ;

    public static GitApiInterface getClient() {
        if (gitApiInterface == null) {

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverter(String.class, new ToStringConverter())
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface ;
    }

    public interface GitApiInterface {

        @Headers("Cache-Control: no-cache")
        @GET("project/sp-rest/index.php/APIv1/Suaraperubahan/kontenAll")
        Call<APIKonten> getKonten(@Query("email") String email);

        @Headers("Cache-Control: no-cache")
        @GET("project/sp-rest/index.php/APIv1/Suaraperubahan/kontenHot")
        Call<APIKonten> getKontenHot(@Query("email") String email);

        @Headers("Cache-Control: no-cache")
        @GET("project/sp-rest/index.php/APIv1/Suaraperubahan/getHistory")
        Call<APIHistory> getHistory(@Query("email") String email);

        @FormUrlEncoded
        @POST("project/sp-rest/index.php/APIv1/Suaraperubahan/signUp")
        Call<APIBaseResponse> signUp(@Field("email") String email, @Field("password") String password);

        @FormUrlEncoded
        @POST("project/sp-rest/index.php/APIv1/Suaraperubahan/login")
        Call<APIUserData> login(@Field("email") String email, @Field("password") String password);

        @FormUrlEncoded
        @POST("project/sp-rest/index.php/APIv1/Suaraperubahan/voteUp")
        Call<APIBaseResponse> voteUp(@Field("email") String email, @Field("idpetisi") String idpetisi);

        @FormUrlEncoded
        @POST("project/sp-rest/index.php/APIv1/Suaraperubahan/voteNormal")
        Call<APIBaseResponse> voteNormal(@Field("email") String email, @Field("idpetisi") String idpetisi);

        @FormUrlEncoded
        @POST("project/sp-rest/index.php/APIv1/Suaraperubahan/addKonten")
        Call<APIBaseResponse> addKonten(@Field("email") String email,
                                        @Field("judulpetisi") String judulpetisi,
                                        @Field("isipetisi") String isipetisi,
                                        @Field("gambarpetisi") String gambarpetisi,
                                        @Field("tags") String tags,
                                        @Field("namagambar") String namagambar);

        @Headers("Cache-Control: no-cache")
        @GET("project/sp-rest/index.php/APIv1/Suaraperubahan/komentarAll")
        Call<ApiComment> getComment(@Query("id_petisi") String idpetisi);

        @FormUrlEncoded
        @POST("project/sp-rest/index.php/APIv1/Suaraperubahan/addKomentar")
        Call<APIBaseResponse> postKomentar(@Field("email") String email,
                                           @Field("idpetisi") String idpetisi,
                                           @Field("isikomentar") Editable isikomentar);
    }

}



