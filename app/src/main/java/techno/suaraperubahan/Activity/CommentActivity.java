package techno.suaraperubahan.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.load.Key;
import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.yalantis.phoenix.PullToRefreshView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import techno.suaraperubahan.Adapter.CommentAdapter;
import techno.suaraperubahan.Model.ApiComment;
import techno.suaraperubahan.Model.UserComment;
import techno.suaraperubahan.Preferences.SessionManager;
import techno.suaraperubahan.R;
import techno.suaraperubahan.Rest.RestClient;

/**
 * Created by MR-ROBOT on 11/16/16.
 */

public class CommentActivity extends AppCompatActivity {

    private static final String TAG = "";
    private PullToRefreshView mPullToRefreshView;
    private ListView lvKonten;
    private SessionManager sessions;
    private Call<ApiComment> call;
    private RestClient.GitApiInterface service;
    private CommentAdapter commentAdapter;
    private ArrayList<UserComment> mList =new ArrayList<>();


    String IdPetisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        sessions = new SessionManager(this);

        lvKonten = (ListView) findViewById(R.id.list_view_comment);

        commentAdapter = new CommentAdapter(getApplicationContext(), R.layout.item_comment, mList);

        lvKonten.setAdapter(commentAdapter);

        mPullToRefreshView = (PullToRefreshView) findViewById(R.id.pull_to_refresh_comment);

        mPullToRefreshView.setRefreshing(true);
        fetchData();

        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchData();
                    }
                }, 100);
            }
        });

    }

    public void fetchData(){
        final ProgressDialog progressDialog = new ProgressDialog(CommentActivity.this,
                R.style.ProgressDialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Mengambil Riwayat...");
        progressDialog.show();

IdPetisi = getIntent().getExtras().getString("idpetisi");

        Log.d("CommentActivity", "AmbilKomentar = " + IdPetisi.toString());
        service = RestClient.getClient();
        call = service.getComment(IdPetisi.toString());
        call.enqueue(new Callback<ApiComment>() {
            @Override
            public void onResponse(Response<ApiComment> response) {
                Log.d("HistoryActivity", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    ApiComment result = response.body();
                    Log.d("HistoryActivity", "response = " + new Gson().toJson(result));
                    if (result != null) {

                        mList.clear();

                        List<UserComment> commentItem = result.getKontenData();

                        if(commentItem!=null)
                        {
                            for (UserComment commentItems : commentItem) {
                                mList.add(commentItems);
                                commentAdapter.notifyDataSetChanged();
                            }
                            mPullToRefreshView.setRefreshing(false);
                        }

                        progressDialog.dismiss();
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    mPullToRefreshView.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getApplicationContext(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });

    }
}
