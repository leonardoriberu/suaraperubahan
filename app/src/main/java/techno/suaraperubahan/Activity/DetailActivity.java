package techno.suaraperubahan.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.Arrays;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import techno.suaraperubahan.Model.APIBaseResponse;
import techno.suaraperubahan.Model.APIUserData;
import techno.suaraperubahan.Model.KontenData;
import techno.suaraperubahan.Preferences.SessionManager;
import techno.suaraperubahan.R;
import techno.suaraperubahan.Rest.RestClient;

import static android.provider.Contacts.SettingsColumns.KEY;
import static techno.suaraperubahan.R.id.ShareButtonFB;
import static techno.suaraperubahan.R.id.start;

public class DetailActivity extends AppCompatActivity {
    private final String TAG = DetailActivity.this.getClass().getName();


    private ShareButton shareButtonFB;
    private CallbackManager callbackManager;
    private AccessToken accessToken;
    private ShareDialog sharedialog;


    private String kirimID;

    ImageView DetailImage;
    TextView txtIdPetisi;
    TextView txtJudulDetail;
    TextView txtDetailDetail;
    TextView txtTagsDetail;
    TextView txtTanggalDetail;

    private KontenData data;

    private TextView tvName;

    private Button btnSharePetisi;
    private Button btnViewComment;
    private Button btnSendComment;
    private SessionManager sessions;

    EditText inputcomment;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private Call<APIBaseResponse> callKomentar;
    private RestClient.GitApiInterface service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sessions = new SessionManager(this);

        Intent i = getIntent();
        data = (KontenData) i.getSerializableExtra("KontenDataItem");
        callbackManager = CallbackManager.Factory.create();



        DetailImage = (ImageView) findViewById(R.id.imgDetail);
        txtIdPetisi = (TextView) findViewById(R.id.IdPetisi);
        txtJudulDetail = (TextView) findViewById(R.id.txtJudulDetail);
        txtDetailDetail = (TextView) findViewById(R.id.txtDetailDetail);
        txtTagsDetail = (TextView) findViewById(R.id.txtTagsDetail);
        txtTanggalDetail = (TextView) findViewById(R.id.txtTanggalDetail);

        inputcomment = (EditText) findViewById(R.id.input_comment);

        Glide.with(this).load(data.getGambar_Petisi()).into(DetailImage);
        txtIdPetisi.setText(data.getID_Petisi());
        txtJudulDetail.setText(data.getJudul_Petisi());
        txtDetailDetail.setText(data.getIsi_Petisi());
        txtTagsDetail.setText("Tags : " + data.getTags());
        txtTanggalDetail.setText("Tanggal : " + data.getCreated_at());
        sharedialog = new ShareDialog(this);

        txtIdPetisi.setVisibility(View.INVISIBLE);

        initParameters();
        initViews();

        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null) {
                    Log.d(TAG, "User logged out successfully");
                }
            }
        };
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    public void initParameters() {
        accessToken = AccessToken.getCurrentAccessToken();

    }

    public void initViews() {

        shareButtonFB = new ShareButton(DetailActivity.this);

        btnSharePetisi = (Button) findViewById(R.id.ShareButtonFB);
        btnViewComment = (Button) findViewById(R.id.btnViewCmt);
        btnSendComment = (Button) findViewById(R.id.btnSend);

        if (accessToken != null) {
            getProfileData();
        } else {
          //  tvName.setVisibility(View.INVISIBLE);
        }


        shareButtonFB.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d(TAG, "Media file share successfully");
                Toast.makeText(DetailActivity.this, "Media file share successfully", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "Media Share Cancel");
                Toast.makeText(DetailActivity.this, "Media Share Cancel", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException e) {
                Log.d(TAG, "Error for sharing media");
                Toast.makeText(DetailActivity.this, "Error for sharing media", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        });

        btnSharePetisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("cek", "masuk");
//
//                if(ShareDialog.canShow(ShareLinkContent.class)){
//                    Log.d("cek", "masuk");
//
//                ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
//                        .setContentTitle("Your Title")
//                        .setContentDescription("Your Description")
//                        .setContentUrl(Uri.parse("http://hendririberu.com/suaraperubahan"))
//                        .setImageUrl(Uri.parse(""))
//                        .build();
//
//                sharedialog.show(shareLinkContent);

                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(data.getJudul_Petisi())
                        .setContentDescription(data.getIsi_Petisi())
                        .setContentUrl(Uri.parse("http://hendririberu.com/suaraperubahan"))
                        .build();

                sharedialog.show(linkContent);




//                    Uri uriPhoto = data.getData();
                    //                  Log.d(TAG, "Selected image path :" + uriPhoto.toString());

//
//                    Bitmap bitmap = null;
//
//                    //                       bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uriPhoto));
//                    bitmap = DetailImage.getDrawingCache();
//                    SharePhoto photo = new SharePhoto.Builder().setBitmap(bitmap)
//                            .build();
//                    SharePhotoContent content = new SharePhotoContent.Builder()
//                            .addPhoto(photo).build();
//
//                    shareButtonFB.setShareContent(content);
//                    shareButtonFB.performClick();

               // }

            }

        });

        btnViewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                kirimID = txtIdPetisi.getText().toString();


                Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
                intent.putExtra("idpetisi", kirimID);

                Log.d(TAG, "idpetisinya :" + kirimID.toString());
                startActivity(intent);

            }
        });

        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postKomen();
            }
        });




    }


    public void postKomen() {
        final ProgressDialog progressDialog = new ProgressDialog(DetailActivity.this,
                R.style.ProgressDialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Menupload Komentar...");
        progressDialog.show();
        kirimID = txtIdPetisi.getText().toString();
        service = RestClient.getClient();
        callKomentar = service.postKomentar(sessions.getUserDetails().get(SessionManager.KEY_EMAIL),kirimID,inputcomment.getText());

        callKomentar.enqueue(new Callback<APIBaseResponse>() {
            @Override
            public void onResponse(Response<APIBaseResponse> response) {
                if(response.isSuccess()){
                    APIBaseResponse result = response.body();
                    Log.d("Komen", "response = " + new Gson().toJson(result));
                    if (result != null) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Komentar berhasil ditambahkan",
                                Toast.LENGTH_SHORT).show();
                        inputcomment.setText("");
                        kirimID = txtIdPetisi.getText().toString();


                        Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
                        intent.putExtra("idpetisi", kirimID);

                        Log.d(TAG, "idpetisinya :" + kirimID.toString());
                        startActivity(intent);
                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Gagal menambahkan komentar",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Gagal menambahkan Komentar",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getProfileData() {
        try {
            accessToken = AccessToken.getCurrentAccessToken();
            tvName.setVisibility(View.VISIBLE);
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.d(TAG, "Graph Object :" + object);
                            try {
                                String name = object.getString("name");
                                tvName.setText("Welcome, " + name);

                                Log.d(TAG, "Name :" + name);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,link,birthday,gender,email");
            request.setParameters(parameters);
            request.executeAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Detail Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
