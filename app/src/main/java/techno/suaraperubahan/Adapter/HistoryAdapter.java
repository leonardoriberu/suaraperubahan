package techno.suaraperubahan.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import techno.suaraperubahan.Model.History;
import techno.suaraperubahan.R;

import java.util.ArrayList;

public class HistoryAdapter  extends ArrayAdapter<History> {
    Context context;
    int resLayout;
    ArrayList<History> listHistory;

    TextView tvHistory;
    TextView tvDetailHistory;
    TextView tvTglHistory;
    History navLisHistory;
    ImageView imgHistory;

    public HistoryAdapter(Context context, int resLayout, ArrayList<History> listHistory){
        super(context, resLayout, listHistory);
        this.context=context;
        this.resLayout=resLayout;
        this.listHistory=listHistory;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View v = View.inflate(context,resLayout,null);

        tvHistory = (TextView)v.findViewById(R.id.txtHadiahHistory);
        tvDetailHistory = (TextView)v.findViewById(R.id.txtDetailHistory);
        tvTglHistory = (TextView)v.findViewById(R.id.txtTglHistory);
        imgHistory = (ImageView)v.findViewById(R.id.imgHadiahHistory);

        navLisHistory = listHistory.get(position);

        tvHistory.setText(navLisHistory.getHadiah());
        tvTglHistory.setText(navLisHistory.getRedeemed_at());

        if(navLisHistory.getHadiah().equalsIgnoreCase("Tas Travel")) {
            tvDetailHistory.setText("Tas cocok untuk berpergian");
            imgHistory.setImageResource(R.drawable.tas);
        }
        else if(navLisHistory.getHadiah().equalsIgnoreCase("Tiket Pesawat")) {
            tvDetailHistory.setText("Tiket pulang pergi wilayah Indonesia");
            imgHistory.setImageResource(R.drawable.tiket);
        }
        else if(navLisHistory.getHadiah().equalsIgnoreCase("Liburan")) {
            tvDetailHistory.setText("2 Hari 3 Malam wilayah Indonesia");
            imgHistory.setImageResource(R.drawable.liburan);
        }

        return v;
    }
}
