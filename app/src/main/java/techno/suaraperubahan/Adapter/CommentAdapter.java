package techno.suaraperubahan.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import techno.suaraperubahan.Model.History;
import techno.suaraperubahan.Model.UserComment;
import techno.suaraperubahan.R;

/**
 * Created by MR-ROBOT on 11/17/16.
 */

public class CommentAdapter extends ArrayAdapter<UserComment> {

    TextView tvEmail;
    TextView tvDate;
    TextView tvComment;

    UserComment userComment;

    Context context;
    int resLayout;
    ArrayList<UserComment> mListComment;

    public CommentAdapter(Context context, int resLayout, ArrayList<UserComment> mListComment) {
        super(context, resLayout, mListComment);
        this.context=context;
        this.resLayout=resLayout;
        this.mListComment=mListComment;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(context,resLayout,null);
        tvComment = (TextView) v.findViewById(R.id.txtComment);
        tvDate = (TextView) v.findViewById(R.id.txtdate);
        tvEmail = (TextView) v.findViewById(R.id.txtEmail);

        userComment = mListComment.get(position);

        tvComment.setText(userComment.getIsi_Komentar());
        tvEmail.setText(userComment.getEmail());
        tvDate.setText(userComment.getCreated_At());


        return  v;

    }
}
