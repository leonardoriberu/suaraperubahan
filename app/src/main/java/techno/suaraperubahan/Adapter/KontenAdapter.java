package techno.suaraperubahan.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import techno.suaraperubahan.Model.APIBaseResponse;
import techno.suaraperubahan.Model.KontenData;
import techno.suaraperubahan.Preferences.SessionManager;
import techno.suaraperubahan.R;
import techno.suaraperubahan.Rest.RestClient;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class KontenAdapter extends ArrayAdapter<KontenData> {

    Context context;
    int resLayout;
    List<KontenData> listKonten;
    private Call<APIBaseResponse> call;
    private RestClient.GitApiInterface service;

    SessionManager sessions;
    KontenData navLisKonten;
    ListView listView;

    static class ViewHolderItem{
        TextView tvNama;
        TextView tvLike;
        ImageView imgKonten;
        ImageView loveIcon;
        ImageView loveokIcon;
    }

    public KontenAdapter(Context context, int resLayout, List<KontenData> listKonten, ListView listView){
        super(context, resLayout, listKonten);
        this.context=context;
        this.resLayout=resLayout;
        this.listKonten=listKonten;
        this.listView=listView;
        sessions = new SessionManager(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolderItem viewHolder;

        if(convertView==null){

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(resLayout, null, false);

            viewHolder = new ViewHolderItem();
            viewHolder.tvNama = (TextView)convertView.findViewById(R.id.txtJudul);
            viewHolder.tvLike = (TextView)convertView.findViewById(R.id.txtLike);
            viewHolder.imgKonten = (ImageView)convertView.findViewById(R.id.kontenImage);
            viewHolder.loveIcon = (ImageView)convertView.findViewById(R.id.love);
            viewHolder.loveokIcon = (ImageView)convertView.findViewById(R.id.loveok);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }


        navLisKonten = listKonten.get(position);

        viewHolder.tvNama.setText(navLisKonten.getJudul_Petisi());
        viewHolder.tvLike.setText(navLisKonten.getJumlah_Dukungan() + " Votes");
        Glide.with(getContext()).load(navLisKonten.getGambar_Petisi()).into(viewHolder.imgKonten);

        if(navLisKonten.getFlag().size()!=0) {
            if (navLisKonten.getFlag().get(0).getFlag() == null) {
                viewHolder.loveIcon.setVisibility(View.VISIBLE);
                viewHolder.loveokIcon.setVisibility(View.GONE);
                navLisKonten.setState(0);
            } else {
                if (navLisKonten.getFlag().get(0).getFlag().equalsIgnoreCase("0")) {
                    viewHolder.loveIcon.setVisibility(View.VISIBLE);
                    viewHolder.loveokIcon.setVisibility(View.GONE);
                    navLisKonten.setState(0);
                } else {
                    viewHolder.loveIcon.setVisibility(View.GONE);
                    viewHolder.loveokIcon.setVisibility(View.VISIBLE);
                    navLisKonten.setState(1);
                }
            }
        }
        else
        {
            viewHolder.loveIcon.setVisibility(View.VISIBLE);
            viewHolder.loveokIcon.setVisibility(View.GONE);
            navLisKonten.setState(0);
        }

        viewHolder.loveIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int position = listView.getPositionForView((View) v.getParent());
                navLisKonten = listKonten.get(position);


                viewHolder.loveIcon.setVisibility(View.GONE);
                viewHolder.loveokIcon.setVisibility(View.VISIBLE);
                if(navLisKonten.getState()==0) {
                    int votenum = navLisKonten.getJumlah_Dukungan();
                    votenum++;
                    viewHolder.tvLike.setText(votenum + " Votes");
                }
                else
                {
                    int votenum = navLisKonten.getJumlah_Dukungan();
                    viewHolder.tvLike.setText(votenum + " Votes");
                }

                voteUp();
            }
        });

        viewHolder.loveokIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int position = listView.getPositionForView((View) v.getParent());
                navLisKonten = listKonten.get(position);


                viewHolder.loveIcon.setVisibility(View.VISIBLE);
                viewHolder.loveokIcon.setVisibility(View.GONE);
                if(navLisKonten.getState()==0) {
                    int votenum = navLisKonten.getJumlah_Dukungan();
                    viewHolder.tvLike.setText(votenum + " Votes");
                }
                else
                {
                    int votenum = navLisKonten.getJumlah_Dukungan();
                    votenum--;
                    viewHolder.tvLike.setText(votenum + " Votes");
                }


                voteNormal();
            }
        });



        return convertView;
    }

    public void voteUp()
    {
        service = RestClient.getClient();
        call = service.voteUp(sessions.getUserDetails().get(SessionManager.KEY_EMAIL), navLisKonten.getID_Petisi());
        call.enqueue(new Callback<APIBaseResponse>() {
            @Override
            public void onResponse(Response<APIBaseResponse> response) {
                Log.d("KontenAdapter", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIBaseResponse result = response.body();
                    Log.d("KontenAdapter", "response = " + new Gson().toJson(result));
                    if(result!=null)
                    {
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    public void voteNormal()
    {
        service = RestClient.getClient();
        call = service.voteNormal(sessions.getUserDetails().get(SessionManager.KEY_EMAIL), navLisKonten.getID_Petisi());
        call.enqueue(new Callback<APIBaseResponse>() {
            @Override
            public void onResponse(Response<APIBaseResponse> response) {
                Log.d("KontenAdapter", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIBaseResponse result = response.body();
                    Log.d("KontenAdapter", "response = " + new Gson().toJson(result));
                    if(result!=null)
                    {
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

}

