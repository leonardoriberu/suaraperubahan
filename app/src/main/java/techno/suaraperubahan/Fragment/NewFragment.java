package techno.suaraperubahan.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import techno.suaraperubahan.Activity.DetailActivity;
import techno.suaraperubahan.Adapter.KontenAdapter;
import techno.suaraperubahan.Model.APIKonten;
import techno.suaraperubahan.Model.KontenData;
import techno.suaraperubahan.Preferences.SessionManager;
import techno.suaraperubahan.R;
import techno.suaraperubahan.Rest.RestClient;
import com.yalantis.phoenix.PullToRefreshView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class NewFragment extends Fragment {

    private Call<APIKonten> call;
    private RestClient.GitApiInterface service;
    private PullToRefreshView mPullToRefreshView;
    private ListView lvKonten;
    private KontenAdapter kontenAdapter;
    private List<KontenData> kontenItems = new ArrayList<KontenData>();;
    private SessionManager sessions;

    public NewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_new, container, false);

        sessions = new SessionManager(getActivity());

        mPullToRefreshView = (PullToRefreshView) v.findViewById(R.id.pull_to_refresh);

        lvKonten = (ListView) v.findViewById(R.id.list_view_konten);

        kontenAdapter = new KontenAdapter(getContext(), R.layout.item_konten, kontenItems, lvKonten);

        lvKonten.setAdapter(kontenAdapter);

        mPullToRefreshView.setRefreshing(true);
        fetchData();

        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchData();
                    }
                }, 100);
            }
        });

        lvKonten.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> kontenitemcurrent, View v, int position,
                                    long id) {

                Intent i = new Intent(getActivity(), DetailActivity.class);
                i.putExtra("KontenDataItem",(KontenData) kontenitemcurrent.getItemAtPosition(position));
                startActivity(i);
            }
        });

        return v;

    }

    public void fetchData()
    {
        service = RestClient.getClient();
        call = service.getKonten(sessions.getUserDetails().get(SessionManager.KEY_EMAIL));
        call.enqueue(new Callback<APIKonten>() {
            @Override
            public void onResponse(Response<APIKonten> response) {
                Log.d("NewFragment", "Status Code = " + response.code());
                if (response.isSuccess()) {
                    // request successful (status code 200, 201)
                    APIKonten result = response.body();
                    Log.d("NewFragment", "response = " + new Gson().toJson(result));
                    if (result != null) {

                        kontenItems.clear();

                        List<KontenData> kontenResponseItems = result.getKontenData();

                        for (KontenData kontenResponseItem : kontenResponseItems) {
                            kontenItems.add(kontenResponseItem);
                            kontenAdapter.notifyDataSetChanged();
                        }

                        mPullToRefreshView.setRefreshing(false);
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Toast.makeText(getActivity(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                    mPullToRefreshView.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Koneksi Ke Internet Gagal", Toast.LENGTH_SHORT).show();
                mPullToRefreshView.setRefreshing(false);
            }
        });

    }



}